from django.shortcuts import render, get_object_or_404, redirect
from .forms import TodoListForm
from .models import TodoList
# Create your views here.


def todo_list_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list": todos,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id):
    list_details = get_object_or_404(TodoList, id=id)
    context = {
        "list_details": list_details
    }
    return render(request, "todos/detail.html", context)


def todo_list_create(request):
    if request.method =="POST":

        form = TodoListForm(request.POST)
        if form.is_valid():
            model_instance = form.save(commit=False)
            model_instance.user = request.user
            model_instance.save()
            return redirect("detail_url", id=model_instance.id)
    else:
        form = TodoListForm()
    context = {
            "form": form
    }
    return render(request, "todos/create.html", context)




def todo_list_update(request, id):
    model_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=model_instance)
        if form.is_valid():
            model_instance = form.save()
            return redirect("detail_url", id=model_instance.id)
    else:
        form = TodoListForm(instance=model_instance)

    context = {
        "form": form
    }

    return render(request, "todos/edit.html", context)
